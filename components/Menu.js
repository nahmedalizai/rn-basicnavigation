import React from 'react';
import {StyleSheet, View, Text, Button, Image} from 'react-native';
import { 
    createSwitchNavigator,
    createDrawerNavigator, 
    createStackNavigator, 
    createAppContainer,
    createBottomTabNavigator,
    DrawerItems
 } from 'react-navigation';
 import {
     Container,
     Header,
     Body, 
     Content, 
     Icon
} from 'native-base'

import SettingsScreen from '../screens/SettingsScreen'
import Screen1 from '../screens/HomeFirstScreen'
import Screen2 from '../screens/HomeSecondScreen'
import DetailScreen from '../screens/DetailScreen'
import LoginScreen from '../screens/LoginScreen'

const DrawerNavigationCustomComponent = (props) => (
    <Container>
        <Image
                source={require('../assets/divided-heart.gif')}
        />
        <Content>
            <DrawerItems {...props}/>
        </Content>
    </Container>
)

const BookStackNavigator = createStackNavigator({
    Book: {screen:Screen1},
    Details: {screen:DetailScreen},
});

const BottomTabNavigator = createBottomTabNavigator ({
        Books: BookStackNavigator,
        Audios: {screen: Screen2},
    },
    {
        navigationOptions: ({navigation})=> {
            const {routeName} = navigation.state.routes
            [navigation.state.index];
            return {
                headerTitle: routeName
            };
        }
    }
);

const StackNavigator = createStackNavigator({
        BottomTabNavigator:BottomTabNavigator,
    },
    {
        defaultNavigationOptions: ({navigation}) => {
            return {
                headerLeft: <Icon 
                                name='menu' 
                                style={{paddingLeft:10}}
                                onPress={() => navigation.openDrawer()} />,
                headerRight: <Icon
                                name='ios-power' 
                                style={{paddingRight:10}}
                                onPress={() => navigation.navigate('Login')} />
                
            };
        }
    }
);

const DrawerNavigator = createDrawerNavigator({
    Home: StackNavigator,
    Settings: SettingsScreen
},
{
    initialRouteName: "Home",
    contentComponent: DrawerNavigationCustomComponent,
    drawerOpenRoute: 'drawerOpen',
    drawerCloseRoute: 'drawerClose',
    drawerToggleRoute: 'drawerToggle',
});

const SwitchNavigator = createSwitchNavigator({
    Login: {screen: LoginScreen},
    Dashboard: {screen: DrawerNavigator},
});

const Menu = createAppContainer(SwitchNavigator);

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });
  

export default Menu;