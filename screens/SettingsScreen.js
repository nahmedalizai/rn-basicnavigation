import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {Header, Left, Right, Body, Title, Icon,} from 'native-base'

export default function SettingsScreen(props) {
  return (
    <View style={styles.container}>
      <Header>
        <Left>
            <Icon 
                name='menu'
                style={{paddingLeft:5}}
                onPress={() => props.navigation.openDrawer()} />
        </Left>
          <Body>
            <Title>Settings</Title>
          </Body>
        <Right>
            <Icon 
                name='ios-power'
                style={{paddingRight:0}}
                onPress={() => props.navigation.navigate('Login')} />
        </Right>
      </Header>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
