import React from 'react';
import { StyleSheet, Text,Button, View } from 'react-native';
export default function HomeFirstScreen(props) {
  return (
    <View style={styles.container}>
      <Button 
            title = "View details"
            onPress={()=> props.navigation.navigate("Details")}
        />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
